// Exam 1 - Practical
// Ian Gruender

#include <iostream>
#include <conio.h>

using namespace std;

float Square(float num1);
float Cube(float num1);

int main()
{
	string again = "y";

	while (again == "y")
	{
		float num1 = 0;
		int num2 = 0;

		cout << "Please enter a number\n";
		cin >> num1;
		cout << "Please enter either square(2) or cube(3) to power the number\n";
		cin >> num2;
		if (num2 == 2)
		{
			cout << "Answer = " << Square(num1) << "\n";
		}
		if (num2 == 3)
		{
			cout << "Answer = " << Cube(num1) << "\n";
		}
		if (num2 != 2 && num2 != 3)
		{
			cout << "Please enter either square(2) or cube(3) to power the number\n";
			cin >> num2;
		}

		cout << "Would you like to play again? (y/n)\n";
		cin >> again;
	}
	if (again == "n")
	{
		cout << "Buh-bye";
	}

	(void)_getch();
	return 0;
}

float Square(float num1)
{
	return num1 * num1;
}
float Cube(float num1)
{
	return num1 * num1 * num1;
}